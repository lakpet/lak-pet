When I look into the eyes of an animal I do not see an animal. I see a living being. I see a friend. I feel a soul. Since they can't speak, let us become their voice!
